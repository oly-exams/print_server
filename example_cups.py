import cups

conn = cups.Connection ()
printers = conn.getPrinters ()
for printer in printers:
    print(printer, printers[printer]["device-uri"])

fname = 'T2-Q_Theory_2_Question.pdf'

conn.printFile('HP-Color-LaserJet-E55040', fname, 'test print', {})
conn.printFile('HP-Color-LaserJet-E55040-2', fname, 'test print', {})
conn.printFile('HP-Color-LaserJet-Flow-E57540', fname, 'test print', {})
