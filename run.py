from flask import Flask, request, jsonify
from werkzeug.utils import secure_filename
import os
import glob
import json
import logging
import printer

app = Flask(__name__)
app.config.from_envvar('IPHOPRINT_SETTINGS')

ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'ps', 'png', 'jpg', 'jpeg'])
API_SECRET = app.config.get('API_SECRET', 'secretAPIKey')

UPLOAD_FOLDER = app.config.get('UPLOAD_FOLDER', 'uploads')
WITH_COVER = app.config.get('WITH_COVER', False)

FILES_TO_KEEP = 100

if not app.debug:
    handler = logging.StreamHandler()
    handler.setLevel(logging.WARNING)
    app.logger.addHandler(handler)

printlog = logging.getLogger('print_server.printjob')
if app.config.get('PRINT_LOG', '-') == '-':
    ch = logging.StreamHandler()
else:
    ch = logging.FileHandler(app.config.get('PRINT_LOG'))
formatter = logging.Formatter('[%(asctime)s - %(name)s] - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
printlog.addHandler(ch)
printlog.setLevel(logging.INFO)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


def delete_old_files(directory):
    # Get the list of files in the directory
    files = glob.glob(os.path.join(directory, '*'))
    files = [f for f in files if os.path.isfile(f)]

    # Sort the files based on modification time
    files.sort(key=os.path.getmtime)

    # Delete all files except the x most recent ones
    for file in files[:-FILES_TO_KEEP]:
        try:
            os.remove(file)
            app.logger.debug(f"Deleted file: {file}")
        except OSError:
            app.logger.warning(f"Unable to delete old file: {file}")

           
@app.route('/')
def index():
    return '<h1>IPhO Print Server</h1>'

@app.route('/print/<queue>', methods=['POST'])
def print_post(queue):
    auth = request.headers.get('Authorization')
    if auth != 'IPhOToken '+API_SECRET:
        return jsonify(status='error', message='Wrong Auth token.'), 403

    if not queue in app.config['PRINTERS_MAP']:
        return jsonify(status='error', message='Queue not configured.'), 400
    printer_queue = app.config['PRINTERS_MAP'][queue]

    queue_dir = os.path.join(UPLOAD_FOLDER, secure_filename(queue))
    if not os.path.exists(queue_dir):
        os.mkdir(queue_dir)
    delete_old_files(queue_dir)
    
    user = request.form.get('user', 'ukn')
    try:
        print(request.form.get('opts', '{}'))
        opts = json.loads(request.form.get('opts', '{}'))
    except:
        opts = {}
    app.logger.debug('Print options: {}'.format(opts))
    title = json.loads(request.form.get('title', '"IPhO Print"'))
    add_banner_page = json.loads(request.form.get('add_banner_page', 'false'))
    app.logger.debug('Print title: {}'.format(title))

    file = request.files['file']
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        save_path = os.path.join(queue_dir, filename)
        file.save(save_path)
        app.logger.info('{} saved in {}'.format(file.filename, save_path))
        printlog.info('{} - {} - {}'.format(printer_queue, user, file.filename))
        printlog.info('{}'.format(opts))
        try:
            printer.print_document(queue=printer_queue, fname=save_path, opts=opts, title=title, add_banner_page=add_banner_page)
        except Exception as e:
            printlog.exception(e)
            return jsonify(status='error', message='Error while communicating with print system (CUPS); please refer to local log on print gateway.'), 400
        return jsonify(status='success')

    return jsonify(status='error', message='File upload failed: no file provided or file type not one of {}'.format(ALLOWED_EXTENSIONS)), 400

if __name__ == "__main__":
    app.run(debug=True)
