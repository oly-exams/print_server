#!/bin/bash

PORT=60006
REMOTE_HOST=printserver@ipho2023-test.oly-exams.org

autossh -M 20000 -o "ServerAliveInterval 30" -o "ServerAliveCountMax 3" -o "ExitOnForwardFailure yes" -R $PORT:localhost:80 $REMOTE_HOST
