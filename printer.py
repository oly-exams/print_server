import io, sys
import os

import cups
from fpdf import FPDF
from pypdf import PdfWriter


def create_banner_page(title):
    pdf = FPDF()
    pdf.add_page()
    pdf.set_font('helvetica', size=16)
    pdf.ln(50)      # move the cursor down by 5 cm
    pdf.cell(txt=title, align='C', center=True) 
    return io.BytesIO(pdf.output())


def insert_banner_page(fn, title):
    name, ext = os.path.splitext(fn)
    output_fn = name + '_banner' + ext
    
    merger = PdfWriter()

    with open(fn, "rb") as input, open(output_fn, "wb") as output:
        merger.append(create_banner_page(title))
        merger.append(input)

        # Write to an output PDF document
        merger.write(output)
    return output_fn


def print_document(queue, fname, opts={}, title='IPhO Print', add_banner_page=False):
    default_opts = {
        'ColourModel': 'Colour',
        'Duplex': 'None',
        # 'Staple': '1PLU',
        'fit-to-page': 'True',
    }
    default_opts.update(opts)
    if add_banner_page:
        name, ext = os.path.splitext(fname)
        if ext.lower() == '.pdf':
            # add banner page using PDF merger
            fname = insert_banner_page(fname, title)
        else:
            # try to add banner page using CUPS (did not work for some printers)
            default_opts.update({'job-sheets': 'standard'})

    conn = cups.Connection ()
    conn.printFile(queue, fname, title, default_opts)
