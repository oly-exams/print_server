Hereafter is described in some detail the setup used during APhO18.


## Printers

Set printers on static local IP addresses.
- Hint: printers often have a remote configuration interface. Simply navigate to their IP in a browser.
- Hint: If you do not know the admin password of the printers, you may want to simply try the default ones... GIYF.

Installed drivers on the print server laptop.

Installed network printers in CUPS (`localhost:631`).

Configured some of the printers to accept jobs from the (local) IP of the print server only.


## Exam Tools deployment

The Exam Tools deployment setup the user `printserver` and installs all the sshkeys of the Oly Exams laptops.

In the configurations, the host of the printers should be `localhost:60006` and `localhost:60007`, see tunnel configuration below.


### Manual server setup

Installed the parameters for the print server by hand (note: would be nice to have in deployment):

**TREAD WITH CARE**: messing with the ssh configuration can kill access to an instance. If the restart does not work, **do not close the connection** and restore the config file.

- connect via ssh (ubuntu user)
- su
- adduser printserver
- nano /etc/ssh/sshd_config
- add line:

```
ClientAliveInterval 50
```

**Note:** this line should be written before any Match-block (not supported inside Match in currently used sshd version)


- service sshd restart

Note: would have been nice to jail the printserver user. Following attempt was made without success (in sshd_config):

```
Match User printserver:
  ChrootDirectory %h
```


## Print server deployment

### Inventory
Usually the print server is installed on the `localhost`, i.e. one of the OlyExams laptops.

### Vars

See group_vars/apho2018_ptl3.yml.

- The `API_SECRET` should match the one used in the `PRINTER_QUEUES` dictionary in exam-tools (set in the deployment).
- The keys in `PRINTERS_MAP` correspond to the values of "queue" set in `PRINTER_QUEUES`.
- The values in `PRINTERS_MAP` correspond to the queue names set in CUPS.


### Run the playbook

```
ansible-playbook --ask-sudo-pass -i inventory/apho2018_ptl3 deploy.yml
```


## Tunnel

Installed autossh.

```
autossh -M 20000 -o "ServerAliveInterval 30" -o "ServerAliveCountMax 3" -o "ExitOnForwardFailure yes" -R 60006:localhost:80 printserver@apho2018.oly-exams.org
```

Note: all settings inclusive the ClientAliveInterval in sshd_config of the main server were important in order for the reconnection mechanism of autossh to work.
However, the delays were maybe set too short, resulting in many losses of connection of the underlying ssh. But autossh playing its role.


### Tunnel monitoring

The option `-M 20000` helps (a bit) monitoring the ssh connection:

- from another local computer, ssh to the print server (the pubkey of that computer has to be installed in the authorized keys of the print server laptop first)
- there, run "ssh localhost -p 20000"
- this call hangs until the ssh in autossh looses connection
- once autossh re-establishes the ssh connection (should be quite fast), the call can be executed again
- thus, one can monitor the losses of connection of ssh.

Note: ideally, this check could be used in a Python script to better monitor the status... if useful.
Note: during APhO2018, both apho2018-test.oly-exams.org and apho2018.oly-exams.org were bound to the print server, requiring most of the settings described on this page to be set on both of them. We used ports 20000 and 20002 for the monitoring (other ports do not need to be set differently).


## Print server printing monitoring

To monitor the printing of jobs, ssh to the print server (again, a pubkey has to be installed on it), then run:

```
tail -f /var/log/iphoprint.printjobs.log
```

The list displayed will update when jobs get printed.


### testing the print server

First upload a file to the exam tools server:

```
scp T2-Q_Theory_2_Question.pdf ubuntu@apho2018-test.oly-exams.org:T2-Q_Theory_2_Question.pdf
```

Then ssh to that server and send the file for printing:

```
curl -X POST -H "Content-Type: multipart/form-data" -H "Authorization: IPhOToken cm932m4c234asdasd83" -F "file=@T2-Q_Theory_2_Question.pdf" http://localhost:60006/print/printer-2
```
