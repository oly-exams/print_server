Hereafter is described in some detail the setup used during APhO18.


## printers

Set printers on static local IP addresses (192.168.X.X).
printers had a remote configuration interface. Simply navigate to their IP in a browser.
credentials were admin:123456

Used LPD/LPR Host ... Ubuntu automatically recognized TOSHIBA e-studio 5055C and 4555C and installed drivers

checking the names with "python example_cups.py"

Configured some of the printers to accept jobs from the (local) IP of the print server only.


## exam-tools deployment

Installed ssh pubkey of the print server in the deployment.
We did this by having a folder pubkeys in the homedirectory of the instance and each file named after the machine with the pubkey

Used `localhost:60006` as the host for all exam-site printers
Used `localhost:60007` as the host for all translation room printers

## exam-tools server

(Michele automated these steps within deploy)
Old: Installed the parameters for the print server by hand (note: would be nice to have in deployment):

Old:**TREAD WITH CARE**: messing with the ssh configuration can kill access to an instance. If the restart does not work, **do not close the connection** and restore the config file.

Old:- connect via ssh (ubuntu user)
Old:- su
Old:- adduser printserver
Old:- nano /etc/ssh/sshd_config
Old:- add line:

Old:```
Old:ClientAliveInterval 50
Old:```

Old:**Note:** this line should be written before any Match-block (not supported inside Match in currently used sshd version)


Old:- service sshd restart

Old:Note: would have been nice to jail the printserver user. Following attempt was made without success (in sshd_config):

Old:```
Old:Match User printserver:
Old:  ChrootDirectory %h
Old:```


## print server deployment inventory

See inventory/apho2019_translation_room.


## print server deployment group_vars

See group_vars/apho2019_translation_room.yml.

The API_SECRET should match the one used in the PRINTER_QUEUES dictionary in exam-tools (set in the deply).

The keys in PRINTERS_MAP correspond to the values of "queue" set in PRINTER_QUEUES.
The values in PRINTERS_MAP correspond to the queue names set in CUPS.


## playbook run

```
ansible-playbook --ask-sudo-pass -i inventory/apho2019_translation_room deploy.yml
```

## testing locally

curl  -X POST -H "Content-Type: multipart/form-data"  -H "Authorization: IPhOToken cm932m4c234asdasd83" -F "file=@T2-Q_Theory_2_Question.pdf" http://127.0.0.1:5000/print/printrelease-1


## tunnel

Installed autossh.

```
autossh -M 20000 -o "ServerAliveInterval 30" -o "ServerAliveCountMax 3" -o "ExitOnForwardFailure yes" -R 60006:localhost:80 printserver@apho2019.oly-exams.org

```
For IPHO2019:
```
autossh -M 20000 -o "ServerAliveInterval 30" -o "ServerAliveCountMax 3" -o "ExitOnForwardFailure yes" -R 60006:localhost:80 printserver@ipho2019.oly-exams.org
```

Note: all settings inclusive the ClientAliveInterval in sshd_config of the main server were important in order for the reconnection mechanism of autossh to work.
However, the delays were maybe set too short, resulting in many losses of connection of the underlying ssh. But autossh playing its role.


## tunnel monitoring

The option `-M 20000` helps (a bit) monitoring the ssh connection:

- from another local computer, ssh to the print server (the pubkey of that computer has to be installed in the authorized keys of the print server laptop first)
- there, run "ssh localhost -p 20000"
- this call hangs until the ssh in autossh looses connection
- once autossh re-establishes the ssh connection (should be quite fast), the call can be executed again
- thus, one can monitor the losses of connection of ssh.

Note: ideally, this check could be used in a Python script to better monitor the status... if useful.
Note: during APhO2018, both apho2018-test.oly-exams.org and apho2018.oly-exams.org were bound to the print server, requiring most of the settings described on this page to be set on both of them. We used ports 20000 and 20002 for the monitoring (other ports do not need to be set differently).


## print server printing monitoring

To monitor the printing of jobs, ssh to the print server (again, a pubkey has to be installed on it), then run:

```
tail -f /var/log/iphoprint.printjobs.log
```

The list displayed will update when jobs get printed.


## testing the print server

First upload a file to the exam tools server:

```
scp T2-Q_Theory_2_Question.pdf ubuntu@apho2018-test.oly-exams.org:T2-Q_Theory_2_Question.pdf
```

For IPHO 2019:
```
scp T2-Q_Theory_2_Question.pdf printserver@ipho2019-test.oly-exams.org:~/.ssh/T2-Q_Theory_2_Question.pdf
```


Then ssh to that server and send the file for printing:

```
curl -X POST -H "Content-Type: multipart/form-data" -H "Authorization: IPhOToken cm932m4c234asdasd83" -F "file=@T2-Q_Theory_2_Question.pdf" http://localhost:60006/print/color-1
```
